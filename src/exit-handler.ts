import { Logger } from '@nestjs/common';

interface ExitListener {
  cleanup?: boolean;
  exit?: boolean;
}

/** Catch process exceptions and exists */
function exitHandler(options: ExitListener, exitCode: number) {
  if (options.cleanup) {
    Logger.log('exit: clean');
  }
  if (exitCode || exitCode === 0) {
    if (exitCode !== 0) {
      Logger.error(`exit: code - ${exitCode}`);
    } else {
      Logger.log(`exit: code - ${exitCode}`);
    }
  }
  if (options.exit) {
    process.exit();
  }
}
// app is closing
process.on('exit', (exitCode: number) => {
  exitHandler({ cleanup: true }, exitCode);
});

// catches ctrl+c event
process.on('SIGINT', (exitCode: number) => {
  exitHandler({ exit: true }, exitCode);
});

// catches "kill pid"
process.on('SIGUSR1', (exitCode: number) => {
  exitHandler({ exit: true }, exitCode);
});
process.on('SIGUSR2', (exitCode: number) => {
  exitHandler({ exit: true }, exitCode);
});

// catches uncaught exceptions
process.on('uncaughtException', (exitCode: number) => {
  exitHandler({ exit: false }, exitCode);
});
