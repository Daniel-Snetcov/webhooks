import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm';

import { WebhookRequestAttemptEntity } from './webhook-request-attempt.entity';

@Entity()
export class WebhookRequestAttemptLogEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column({ type: 'uuid' })
  @Index()
  webhookRequestAttemptId!: string;

  @ManyToOne(() => WebhookRequestAttemptEntity, (attempt) => attempt.logs)
  @JoinColumn({ name: 'webhookRequestAttemptId' })
  webhookRequestAttempt?: WebhookRequestAttemptEntity;

  @Column({ type: 'integer' })
  statusCode!: number;

  @Column({ type: 'jsonb', nullable: true })
  jsonResponse?: Record<string, unknown>;

  @Column({ type: 'xml', nullable: true })
  xmlResponse?: string;
}
