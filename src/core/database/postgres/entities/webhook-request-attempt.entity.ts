import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';

import { WebhookRequestAttemptLogEntity } from './webhook-request-attempt-log.entity';
import { WebhookEntity } from './webhook.entity';

const REQUESTS_STATUSES = ['PENDING', 'SUCCESS', 'FAILED'] as const;
type RequestStatus = (typeof REQUESTS_STATUSES)[number];

@Entity()
export class WebhookRequestAttemptEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column({ type: 'uuid' })
  webhookId!: string;

  @ManyToOne(() => WebhookEntity, (webhook) => webhook.attempts)
  @JoinColumn({ name: 'webhookId' })
  webhook?: WebhookEntity;

  @OneToMany(
    () => WebhookRequestAttemptLogEntity,
    (attemptLog) => attemptLog.webhookRequestAttempt,
  )
  logs?: WebhookRequestAttemptLogEntity[];

  @Column()
  trigger!: string;

  @Column({
    type: 'enum',
    enum: REQUESTS_STATUSES,
    default: 'PENDING' as RequestStatus,
  })
  status!: RequestStatus;

  @Column({ default: 0 })
  retriedTimes!: number;

  @Column({ type: 'jsonb', nullable: true })
  body?: Record<string, unknown>;
}
