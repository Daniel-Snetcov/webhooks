import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm';

import { WebhookEntity } from './webhook.entity';

@Entity()
export class WebhookHeaderEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column({ type: 'uuid' })
  @Index()
  webhookId!: string;

  @ManyToOne(() => WebhookEntity, (webhook) => webhook.headers)
  @JoinColumn({ name: 'webhookId' })
  webhook?: WebhookEntity;

  @Column()
  header!: string;

  @Column()
  value!: string;
}
