import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

import { WebhookHeaderEntity } from './webhook-header.entity';
import { WebhookRequestAttemptEntity } from './webhook-request-attempt.entity';
import { WebhookTriggerEntity } from './webhook-trigger.entity';

@Entity()
export class WebhookEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column()
  url!: string;

  @Column({ default: 3 })
  maxRetryTimes!: number;

  @OneToMany(() => WebhookHeaderEntity, (header) => header.webhook, {
    eager: true,
  })
  headers?: WebhookHeaderEntity[];

  @OneToMany(() => WebhookTriggerEntity, (trigger) => trigger.webhook, {
    eager: true,
  })
  triggers?: WebhookTriggerEntity[];

  @OneToMany(() => WebhookRequestAttemptEntity, (attempt) => attempt.webhook)
  attempts?: WebhookRequestAttemptEntity[];
}
