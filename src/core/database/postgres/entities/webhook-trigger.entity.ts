import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  Column,
  JoinColumn,
  Index,
} from 'typeorm';

import { TriggerEntity } from './trigger.entity';
import { WebhookEntity } from './webhook.entity';

@Entity()
@Index(['webhookId', 'triggerId'], { unique: true })
export class WebhookTriggerEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column({ type: 'uuid' })
  @Index()
  triggerId!: string;

  @ManyToOne(() => TriggerEntity, (trigger) => trigger.webhooks)
  @JoinColumn({ name: 'triggerId' })
  trigger?: TriggerEntity;

  @Column({ type: 'uuid' })
  @Index()
  webhookId!: string;

  @ManyToOne(() => WebhookEntity, (webhook) => webhook.triggers)
  @JoinColumn({ name: 'webhookId' })
  webhook?: WebhookEntity;
}
