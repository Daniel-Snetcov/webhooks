import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  Index,
} from 'typeorm';

import { WebhookTriggerEntity } from './webhook-trigger.entity';

@Entity()
export class TriggerEntity {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  @DeleteDateColumn()
  deletedAt!: Date;

  @Column()
  @Index()
  name!: string;

  @Column({ nullable: true })
  description?: string;

  @OneToMany(
    () => WebhookTriggerEntity,
    (webhookTrigger) => webhookTrigger.trigger,
  )
  webhooks?: WebhookTriggerEntity[];
}
