import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1698935400302 implements MigrationInterface {
  name = 'Init1698935400302';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "webhook_header_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "webhookId" uuid NOT NULL, "header" character varying NOT NULL, "value" character varying NOT NULL, CONSTRAINT "PK_8eec11d3ac4f50b758aa6ec6258" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_a42386bc3bb7f2b21364f96b6a" ON "webhook_header_entity" ("webhookId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "webhook_request_attempt_log_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "webhookRequestAttemptId" uuid NOT NULL, "statusCode" integer NOT NULL, "jsonResponse" jsonb, "xmlResponse" xml, CONSTRAINT "PK_fcf99706b50e9f0f005b85f1e67" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_5e85d147f0404944267dd145fd" ON "webhook_request_attempt_log_entity" ("webhookRequestAttemptId") `,
    );
    await queryRunner.query(
      `CREATE TYPE "public"."webhook_request_attempt_entity_status_enum" AS ENUM('PENDING', 'SUCCESS', 'FAILED')`,
    );
    await queryRunner.query(
      `CREATE TABLE "webhook_request_attempt_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "webhookId" uuid NOT NULL, "trigger" character varying NOT NULL, "status" "public"."webhook_request_attempt_entity_status_enum" NOT NULL DEFAULT 'PENDING', "retriedTimes" integer NOT NULL DEFAULT '0', "body" jsonb, CONSTRAINT "PK_c139fc4fafbb576f5d07025d8ba" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "webhook_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "url" character varying NOT NULL, "maxRetryTimes" integer NOT NULL DEFAULT '3', CONSTRAINT "PK_454d0cfa6b36d1003372b318ce7" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "webhook_trigger_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "triggerId" uuid NOT NULL, "webhookId" uuid NOT NULL, CONSTRAINT "PK_4b6218db859673c97c7e435c459" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_ddde3f31e6e95458e2ff5440b5" ON "webhook_trigger_entity" ("triggerId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_c3d6d288cd67fd2c7b781bc66f" ON "webhook_trigger_entity" ("webhookId") `,
    );
    await queryRunner.query(
      `CREATE UNIQUE INDEX "IDX_07b466e80e266e2d09c4cb94c7" ON "webhook_trigger_entity" ("webhookId", "triggerId") `,
    );
    await queryRunner.query(
      `CREATE TABLE "trigger_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "name" character varying NOT NULL, "description" character varying, CONSTRAINT "PK_ec511a8ce80ccbfdc6caecd91d9" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_61f7fbb9ad08e67ae51a79a8cb" ON "trigger_entity" ("name") `,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_header_entity" ADD CONSTRAINT "FK_a42386bc3bb7f2b21364f96b6a3" FOREIGN KEY ("webhookId") REFERENCES "webhook_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_request_attempt_log_entity" ADD CONSTRAINT "FK_5e85d147f0404944267dd145fde" FOREIGN KEY ("webhookRequestAttemptId") REFERENCES "webhook_request_attempt_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_request_attempt_entity" ADD CONSTRAINT "FK_7a48ffea5f50a1294829f2540e5" FOREIGN KEY ("webhookId") REFERENCES "webhook_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_trigger_entity" ADD CONSTRAINT "FK_ddde3f31e6e95458e2ff5440b50" FOREIGN KEY ("triggerId") REFERENCES "trigger_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_trigger_entity" ADD CONSTRAINT "FK_c3d6d288cd67fd2c7b781bc66f0" FOREIGN KEY ("webhookId") REFERENCES "webhook_entity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "webhook_trigger_entity" DROP CONSTRAINT "FK_c3d6d288cd67fd2c7b781bc66f0"`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_trigger_entity" DROP CONSTRAINT "FK_ddde3f31e6e95458e2ff5440b50"`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_request_attempt_entity" DROP CONSTRAINT "FK_7a48ffea5f50a1294829f2540e5"`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_request_attempt_log_entity" DROP CONSTRAINT "FK_5e85d147f0404944267dd145fde"`,
    );
    await queryRunner.query(
      `ALTER TABLE "webhook_header_entity" DROP CONSTRAINT "FK_a42386bc3bb7f2b21364f96b6a3"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_61f7fbb9ad08e67ae51a79a8cb"`,
    );
    await queryRunner.query(`DROP TABLE "trigger_entity"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_07b466e80e266e2d09c4cb94c7"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_c3d6d288cd67fd2c7b781bc66f"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_ddde3f31e6e95458e2ff5440b5"`,
    );
    await queryRunner.query(`DROP TABLE "webhook_trigger_entity"`);
    await queryRunner.query(`DROP TABLE "webhook_entity"`);
    await queryRunner.query(`DROP TABLE "webhook_request_attempt_entity"`);
    await queryRunner.query(
      `DROP TYPE "public"."webhook_request_attempt_entity_status_enum"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_5e85d147f0404944267dd145fd"`,
    );
    await queryRunner.query(`DROP TABLE "webhook_request_attempt_log_entity"`);
    await queryRunner.query(
      `DROP INDEX "public"."IDX_a42386bc3bb7f2b21364f96b6a"`,
    );
    await queryRunner.query(`DROP TABLE "webhook_header_entity"`);
  }
}
