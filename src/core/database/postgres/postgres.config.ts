import { parseNumber } from '@common/utils';
import { registerAs } from '@nestjs/config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const postgresConfig = registerAs(
  'postgres',
  (): TypeOrmModuleOptions => {
    return {
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: parseNumber(process.env.POSTGRES_PORT, 5432),
      username: process.env.POSTGRES_USERNAME,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
      schema: process.env.POSTGRES_SCHEMA,
      entities: [`${__dirname}/entities/*.entity{.ts,.js}`],
      migrations: [`${__dirname}/migrations/*{.ts,.js}`],
      migrationsTableName: 'migrations',
      migrationsRun: true,
    };
  },
);
