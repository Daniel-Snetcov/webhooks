import { DataSource, DataSourceOptions } from 'typeorm';

export const datasourceOptions: DataSourceOptions = {
  type: 'postgres',
  host: process.env['POSTGRES_HOST'],
  database: process.env['POSTGRES_DATABASE'],
  schema: process.env['POSTGRES_SCHEMA'],
  username: process.env['POSTGRES_USERNAME'],
  password: process.env['POSTGRES_PASSWORD'],
  port: !process.env['POSTGRES_PORT'] ? 5432 : +process.env['POSTGRES_PORT'],
  synchronize: true,
  logging: true,
  entities: [`${__dirname}/entities/*.entity{.ts,.js}`],
  migrationsTableName: 'migrations',
  migrations: [`${__dirname}/migrations/*{.ts,.js}`],
};

export default new DataSource(datasourceOptions);
