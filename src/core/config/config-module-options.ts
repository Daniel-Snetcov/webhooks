import { jwtConfig } from '@core/auth';
import { postgresConfig } from '@core/database/postgres';
import { ConfigModuleOptions } from '@nestjs/config';

import { appConfig } from './app.config';
import { validationSchema } from './validation-schema';

export const configModuleOptions: ConfigModuleOptions = {
  isGlobal: true,
  validationSchema,
  load: [appConfig, postgresConfig, jwtConfig],
};
