import { NODE_ENV, ProcessEnv } from '@common/types';
import Joi, { SchemaLike } from 'joi';

type ValidationSchema = {
  [P in keyof ProcessEnv]: SchemaLike;
};

const schema: ValidationSchema = {
  NODE_ENV: Joi.string()
    .valid(...NODE_ENV)
    .required(),
  APP_PORT: Joi.number().default(3000),
  APP_CORS_ORIGIN: Joi.string().required(),
  APP_GLOBAL_PREFIX: Joi.string().default('api/v1'),

  JWT_SECRET: Joi.string().required(),
  JWT_EXPIRES_IN: Joi.string().required(),

  POSTGRES_HOST: Joi.string().required(),
  POSTGRES_PORT: Joi.number().default(5432),
  POSTGRES_USERNAME: Joi.string().required(),
  POSTGRES_PASSWORD: Joi.string().required(),
  POSTGRES_DATABASE: Joi.string().required(),
  POSTGRES_SCHEMA: Joi.string().required(),
};

export const validationSchema = Joi.object(schema);
