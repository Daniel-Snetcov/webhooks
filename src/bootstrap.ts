import { AppConfig } from '@core/config';
import { setupSwagger, swaggerPath } from '@core/swagger';
import { AppModule } from '@modules/app.module';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

/** Bootstrap configuration */
export async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  const { port, env, cors, globalPrefix } = config.getOrThrow<AppConfig>('app');

  app.setGlobalPrefix(globalPrefix);
  app.enableCors(cors);
  setupSwagger(app, swaggerPath);

  await app.listen(port);

  Logger.log(
    `Application is running in ${env} mode on: http://localhost:${port}`,
    'NestApplication',
  );
  Logger.log(
    `Documentation can be found on: http://localhost:${port}/${swaggerPath}`,
    'NestApplication',
  );
}
