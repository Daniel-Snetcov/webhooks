export const REQUEST_STATUSES = ['PENDING', 'SUCCESS', 'FAILED'] as const;
export type RequestStatus = (typeof REQUEST_STATUSES)[number];
