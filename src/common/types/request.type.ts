import { UserDto } from '@common/dtos';
import { Request as ExpressRequest } from 'express';

export type Request = ExpressRequest & {
  user: UserDto;
};
