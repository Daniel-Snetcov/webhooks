export class PageDto<T> {
  constructor(
    partial: Pick<PageDto<T>, 'content' | 'take' | 'page' | 'total'>,
  ) {
    Object.assign(this, partial);
    this.totalPages = Math.ceil(partial.total / partial.take);
    this.last = partial.page === this.totalPages;
  }

  content!: T[];

  take!: number;

  page!: number;

  total!: number;

  totalPages!: number;

  last!: boolean;
}
