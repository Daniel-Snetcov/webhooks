import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsUUID } from 'class-validator';
import { randomUUID } from 'crypto';

@Expose()
export class UserDto {
  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;
}
