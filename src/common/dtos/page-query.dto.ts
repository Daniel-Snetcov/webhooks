import { parseNumber } from '@common/utils';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, Transform } from 'class-transformer';
import { IsInt, IsOptional, Max, Min } from 'class-validator';

@Expose()
export class PageQueryDto {
  @ApiProperty({
    type: Number,
    example: 1,
    default: 1,
    required: false,
  })
  @Min(1)
  @IsInt()
  @Transform(({ value }) => parseNumber(value, 1))
  @IsOptional()
  public page = 1;

  @ApiProperty({
    type: Number,
    example: 10,
    default: 10,
    required: false,
  })
  @Max(1000)
  @Min(1)
  @IsInt()
  @Transform(({ value }) => parseNumber(value, 10))
  @IsOptional()
  public take = 10;

  public get skip(): number {
    return (this.page - 1) * this.take;
  }
}
