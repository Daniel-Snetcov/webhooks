const NON_ALPHANUMERIC_CHARACTERS = /[^a-zA-Z0-9]/g;

export function sanitizeString(input: string): string {
  return input.replace(NON_ALPHANUMERIC_CHARACTERS, '');
}
