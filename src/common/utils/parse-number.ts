export function parseNumber<T>(target: T, defaultValue: number) {
  const number = +target;
  return Number.isNaN(number) ? defaultValue : number;
}
