import { Observable, retry, timer } from 'rxjs';

export function backoffRetry<T>(count = 3, delay = 200) {
  return (obs$: Observable<T>) =>
    obs$.pipe(
      retry({
        count,
        delay: (_, retryIndex) => {
          const exponentialBackoff = 2 ** (retryIndex - 1) * delay;
          return timer(exponentialBackoff);
        },
      }),
    );
}
