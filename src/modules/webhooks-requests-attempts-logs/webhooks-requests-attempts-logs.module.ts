import { WebhookRequestAttemptLogEntity } from '@core/database/postgres/entities';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WebhooksRequestsAttemptsLogsService } from './webhooks-requests-attempts-logs.service';

@Module({
  imports: [TypeOrmModule.forFeature([WebhookRequestAttemptLogEntity])],
  providers: [WebhooksRequestsAttemptsLogsService],
  exports: [WebhooksRequestsAttemptsLogsService],
})
export class WebhooksRequestsAttemptsLogsModule {}
