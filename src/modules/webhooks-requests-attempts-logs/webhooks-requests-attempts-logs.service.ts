import { TypeormCrudRepository } from '@common/typeorm';
import { WebhookRequestAttemptLogEntity } from '@core/database/postgres/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WebhooksRequestsAttemptsLogsService extends TypeormCrudRepository<WebhookRequestAttemptLogEntity> {
  constructor(
    @InjectRepository(WebhookRequestAttemptLogEntity)
    readonly webhooksRequestsAttemptsLogsRepository: Repository<WebhookRequestAttemptLogEntity>,
  ) {
    super(webhooksRequestsAttemptsLogsRepository);
  }
}
