import { WebhookRequestAttemptEntity } from '@core/database/postgres/entities';
import { WebhooksModule } from '@modules/webhooks/webhooks.module';
import { WebhooksRequestsAttemptsLogsModule } from '@modules/webhooks-requests-attempts-logs/webhooks-requests-attempts-logs.module';
import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WebhooksRequestsAttemptsController } from './webhooks-requests-attempts.controller';
import { WebhooksRequestsAttemptsService } from './webhooks-requests-attempts.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([WebhookRequestAttemptEntity]),
    HttpModule.register({}),
    WebhooksModule,
    WebhooksRequestsAttemptsLogsModule,
  ],
  controllers: [WebhooksRequestsAttemptsController],
  providers: [WebhooksRequestsAttemptsService],
  exports: [WebhooksRequestsAttemptsService],
})
export class WebhooksRequestsAttemptsModule {}
