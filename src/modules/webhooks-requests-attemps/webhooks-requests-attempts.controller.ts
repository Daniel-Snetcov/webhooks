import { Body, Controller, Param, ParseUUIDPipe, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { WebhookRequestAttemptCreateOneDto } from './dtos';
import { WebhooksRequestsAttemptsService } from './webhooks-requests-attempts.service';

@Controller()
export class WebhooksRequestsAttemptsController {
  constructor(
    private readonly webhooksRequestsAttemptsService: WebhooksRequestsAttemptsService,
  ) {}

  @ApiTags('Webhooks')
  @Post('webhooks/:webhookId/invoke')
  invokeWebhook(
    @Param('webhookId', ParseUUIDPipe) webhookId: string,
    @Body() dto: WebhookRequestAttemptCreateOneDto,
  ) {
    return this.webhooksRequestsAttemptsService.invokeWebhook(webhookId, dto);
  }
}
