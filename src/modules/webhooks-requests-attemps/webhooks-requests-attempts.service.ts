import { TypeormCrudRepository } from '@common/typeorm';
import { backoffRetry, sanitizeString } from '@common/utils';
import {
  WebhookEntity,
  WebhookRequestAttemptEntity,
} from '@core/database/postgres/entities';
import { WebhooksService } from '@modules/webhooks';
import { WebhooksRequestsAttemptsLogsService } from '@modules/webhooks-requests-attempts-logs';
import { HttpService } from '@nestjs/axios';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AxiosResponse } from 'axios';
import { concatMap, map, tap } from 'rxjs';
import { Repository } from 'typeorm';

import { WebhookRequestAttemptCreateOneDto } from './dtos';

@Injectable()
export class WebhooksRequestsAttemptsService extends TypeormCrudRepository<WebhookRequestAttemptEntity> {
  constructor(
    @InjectRepository(WebhookRequestAttemptEntity)
    readonly webhooksRequestsAttemptsRepository: Repository<WebhookRequestAttemptEntity>,
    private readonly webhooksService: WebhooksService,
    private readonly axios: HttpService,
    private readonly webhooksRequestsAttemptsLogsService: WebhooksRequestsAttemptsLogsService,
  ) {
    super(webhooksRequestsAttemptsRepository);
  }

  invokeWebhook(webhookId: string, dto: WebhookRequestAttemptCreateOneDto) {
    let retriedTimes = 0;
    const findWebhook$ = this.webhooksService
      .findOneOrThrow({
        where: { id: webhookId },
        relations: {
          headers: true,
          triggers: {
            trigger: true,
          },
        },
      })
      .pipe(
        tap((webhook) => {
          this.webhookIsSubscribed(webhook, dto.trigger);
        }),
        concatMap((webhook) => {
          return this.createOne({
            webhookId: webhook.id,
            trigger: dto.trigger,
            body: dto as {},
          }).pipe(
            map((webhookRequestAttempt) => {
              return {
                webhookRequestAttemptId: webhookRequestAttempt.id,
                webhook,
              };
            }),
          );
        }),
      );
    return findWebhook$.pipe(
      concatMap(({ webhookRequestAttemptId, webhook }) => {
        return this.findOneOrThrow({
          where: { id: webhookRequestAttemptId },
        }).pipe(
          concatMap((webhookRequestAttempt) => {
            const headers = this.accumulateHeaders(webhook);
            return this.axios
              .post(webhook.url, dto, {
                headers,
                validateStatus: () => true,
              })
              .pipe(
                map((response) => {
                  return {
                    webhookRequestAttempt,
                    response,
                  };
                }),
              );
          }),
          concatMap(({ webhookRequestAttempt, response }) =>
            this.createLog(webhookRequestAttempt, response),
          ),
          map((response) => {
            const isSuccess = response.status >= 200 && response.status < 300;
            if (!isSuccess && retriedTimes < webhook.maxRetryTimes) {
              retriedTimes += 1;
              throw Error('Retry request');
            }
          }),
          concatMap(() => {
            return this.updateOne(
              {
                where: { id: webhookRequestAttemptId },
              },
              {
                retriedTimes,
                status:
                  retriedTimes >= webhook.maxRetryTimes ? 'FAILED' : 'SUCCESS',
              },
            );
          }),
          backoffRetry(webhook.maxRetryTimes, 200),
        );
      }),
    );
  }

  private accumulateHeaders(webhook: WebhookEntity): Record<string, string> {
    return (
      webhook.headers?.reduce(
        (acc, { header, value }) => {
          acc[header] = value;
          return acc;
        },
        {} as Record<string, string>,
      ) ?? {}
    );
  }

  private webhookIsSubscribed(webhook: WebhookEntity, trigger: string) {
    const triggers =
      webhook.triggers?.map((webhookTrigger) =>
        sanitizeString(webhookTrigger.trigger?.name ?? '').toLowerCase(),
      ) ?? [];
    const targetTrigger = sanitizeString(trigger).toLowerCase();
    const webhookSubscribedToTrigger = triggers.includes(targetTrigger);
    if (!webhookSubscribedToTrigger) {
      throw new HttpException(
        'Webhook not subscribed to the trigger',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
  }

  private createLog(
    webhookRequestAttempt: WebhookRequestAttemptEntity,
    response: AxiosResponse<unknown, unknown>,
  ) {
    const xmlResponse =
      typeof response.data === 'string' ? response.data : undefined;
    const jsonResponse =
      typeof response.data === 'object'
        ? (response.data as Record<string, unknown>)
        : undefined;
    return this.webhooksRequestsAttemptsLogsService
      .createOne({
        webhookRequestAttemptId: webhookRequestAttempt.id,
        statusCode: response.status,
        jsonResponse,
        xmlResponse,
      })
      .pipe(map(() => response));
  }
}
