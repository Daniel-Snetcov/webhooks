import { REQUEST_STATUSES, RequestStatus } from '@common/enums';
import {
  WebhookRequestAttemptEntity,
  WebhookRequestAttemptLogEntity,
} from '@core/database/postgres/entities';
import { WebhookDto } from '@modules/webhooks/dtos';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsDate,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { randomUUID } from 'crypto';

type Fields = {
  [P in keyof WebhookRequestAttemptEntity]: WebhookRequestAttemptEntity[P];
};

@Expose()
export class WebhookRequestAttemptDto implements Fields {
  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  createdAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  updatedAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  @Exclude()
  deletedAt!: Date;

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  webhookId!: string;

  @ApiProperty({
    type: WebhookDto,
  })
  @Type(() => WebhookDto)
  @IsOptional()
  webhook?: WebhookDto | undefined;

  logs?: WebhookRequestAttemptLogEntity[] | undefined;

  @ApiProperty({
    example: 'USER_REGISTERED',
  })
  @IsString()
  @IsNotEmpty()
  trigger!: string;

  @ApiProperty({
    type: String,
    enum: REQUEST_STATUSES,
    default: 'PENDING' as RequestStatus,
  })
  @IsEnum(REQUEST_STATUSES)
  @IsNotEmpty()
  status!: RequestStatus;

  @ApiProperty({
    example: 1,
  })
  @IsInt()
  @IsNotEmpty()
  retriedTimes!: number;

  @ApiProperty({
    type: Object,
    example: {
      firstName: 'John',
      lastName: 'Doe',
    },
  })
  @IsObject()
  @IsOptional()
  body?: Record<string, unknown>;
}
