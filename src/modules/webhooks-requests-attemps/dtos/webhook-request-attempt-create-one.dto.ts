import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class WebhookRequestAttemptCreateOneDto {
  @ApiProperty({
    example: 'USER_REGISTERED',
  })
  @IsString()
  @IsNotEmpty()
  trigger!: string;
}
