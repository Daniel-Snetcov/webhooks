import { ApiPaginatedResponse } from '@common/decorators';
import { PageDto, PageQueryDto } from '@common/dtos';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable, forkJoin, map } from 'rxjs';

import { WebhookCreateOneDto, WebhookDto, WebhookUpdateOneDto } from './dtos';
import { WebhooksService } from './webhooks.service';

@ApiTags('Webhooks')
@Controller('webhooks')
export class WebhooksController {
  constructor(private readonly webhooksService: WebhooksService) {}

  @ApiResponse({ type: WebhookDto })
  @Post()
  createOne(@Body() dto: WebhookCreateOneDto): Observable<WebhookDto> {
    return this.webhooksService
      .createOne(dto)
      .pipe(map((entity) => new WebhookDto(entity)));
  }

  @ApiResponse({ type: WebhookDto })
  @Get(':id')
  findOne(@Param('id', ParseUUIDPipe) id: string): Observable<WebhookDto> {
    return this.webhooksService
      .findOneOrThrow({ where: { id } })
      .pipe(map((entity) => new WebhookDto(entity)));
  }

  @ApiPaginatedResponse(WebhookDto)
  @Get()
  findMany(
    @Query() { take, skip, page }: PageQueryDto,
  ): Observable<PageDto<WebhookDto>> {
    const getEntities$ = this.webhooksService
      .findMany({
        take,
        skip,
      })
      .pipe(
        map((entities) => entities.map((entity) => new WebhookDto(entity))),
      );
    const countEntities$ = this.webhooksService.webhooksRepository.count({
      take,
      skip,
    });
    return forkJoin([getEntities$, countEntities$]).pipe(
      map(([content, count]) => {
        return new PageDto({
          content,
          take,
          page,
          total: count,
        });
      }),
    );
  }

  @ApiResponse({ type: WebhookDto })
  @Patch(':id')
  updateOne(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() dto: WebhookUpdateOneDto,
  ): Observable<WebhookDto> {
    return this.webhooksService
      .updateOne({ where: { id } }, dto)
      .pipe(map((entity) => new WebhookDto(entity)));
  }

  @ApiResponse({ type: WebhookDto })
  @Delete(':id')
  removeOne(@Param('id', ParseUUIDPipe) id: string): Observable<WebhookDto> {
    return this.webhooksService
      .removeOne({ where: { id } })
      .pipe(map((entity) => new WebhookDto(entity)));
  }
}
