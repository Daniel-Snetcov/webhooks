import { TypeormCrudRepository } from '@common/typeorm';
import { WebhookEntity } from '@core/database/postgres/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WebhooksService extends TypeormCrudRepository<WebhookEntity> {
  constructor(
    @InjectRepository(WebhookEntity)
    readonly webhooksRepository: Repository<WebhookEntity>,
  ) {
    super(webhooksRepository);
  }
}
