import { PickType } from '@nestjs/swagger';

import { WebhookDto } from './webhook.dto';

export class WebhookCreateOneDto extends PickType(WebhookDto, [
  'url',
  'maxRetryTimes',
] as const) {}
