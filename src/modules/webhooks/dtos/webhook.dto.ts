import { parseNumber } from '@common/utils';
import { WebhookEntity } from '@core/database/postgres/entities';
import { WebhookHeaderDto } from '@modules/webhooks-headers/dtos';
import { WebhookTriggerDto } from '@modules/webhooks-triggers';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsUUID,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { randomUUID } from 'crypto';

type Fields = {
  [P in keyof WebhookEntity]: WebhookEntity[P];
};

@Expose()
export class WebhookDto implements Fields {
  constructor(partial: Partial<WebhookDto>) {
    Object.assign(this, partial);
  }

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  createdAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  updatedAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  @Exclude()
  deletedAt!: Date;

  @ApiProperty({
    example: 'https://www.webhook.com',
  })
  @IsNotEmpty()
  url!: string;

  @ApiProperty({
    example: 5,
    default: 3,
    minimum: 0,
    maximum: 5,
    required: false,
  })
  @Max(5)
  @Min(0)
  @IsInt()
  @Transform(({ value }) => parseNumber(value, 3))
  @IsOptional()
  maxRetryTimes!: number;

  @ApiProperty({
    type: WebhookHeaderDto,
  })
  @ValidateNested({ each: true })
  @Type(() => WebhookHeaderDto)
  @IsArray()
  @IsOptional()
  headers?: WebhookHeaderDto[];

  @ApiProperty({
    type: () => WebhookTriggerDto,
  })
  @ValidateNested({ each: true })
  @Type(() => WebhookTriggerDto)
  @IsArray()
  @IsOptional()
  triggers?: WebhookTriggerDto[] | undefined;
}
