import { PartialType } from '@nestjs/swagger';

import { WebhookCreateOneDto } from './webhook-create-one.dto';

export class WebhookUpdateOneDto extends PartialType(WebhookCreateOneDto) {}
