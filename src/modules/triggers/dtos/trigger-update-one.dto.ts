import { PartialType } from '@nestjs/swagger';

import { TriggerCreateOneDto } from './trigger-create-one.dto';

export class TriggerUpdateOneDto extends PartialType(TriggerCreateOneDto) {}
