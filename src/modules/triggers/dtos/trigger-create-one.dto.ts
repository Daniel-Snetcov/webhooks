import { PickType } from '@nestjs/swagger';

import { TriggerDto } from './trigger.dto';

export class TriggerCreateOneDto extends PickType(TriggerDto, [
  'name',
  'description',
] as const) {}
