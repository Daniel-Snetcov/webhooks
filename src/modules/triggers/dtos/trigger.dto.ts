import {
  TriggerEntity,
  WebhookTriggerEntity,
} from '@core/database/postgres/entities';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { randomUUID } from 'crypto';

type Fields = {
  [P in keyof TriggerEntity]: TriggerEntity[P];
};

@Expose()
export class TriggerDto implements Fields {
  constructor(partial: Partial<TriggerDto>) {
    Object.assign(this, partial);
  }

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  createdAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  updatedAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  @Exclude()
  deletedAt!: Date;

  @ApiProperty({
    example: 'User registered',
  })
  @IsString()
  @IsNotEmpty()
  name!: string;

  @ApiProperty({
    example: 'Get notification when a user registers',
  })
  @IsString()
  @IsOptional()
  description?: string | undefined;

  @ApiProperty({
    type: Object,
  })
  @Type(() => WebhookTriggerEntity)
  @IsArray()
  @IsOptional()
  webhooks?: WebhookTriggerEntity[] | undefined;
}
