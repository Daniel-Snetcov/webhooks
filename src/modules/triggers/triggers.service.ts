import { TypeormCrudRepository } from '@common/typeorm';
import { TriggerEntity } from '@core/database/postgres/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TriggersService extends TypeormCrudRepository<TriggerEntity> {
  constructor(
    @InjectRepository(TriggerEntity)
    readonly triggersRepository: Repository<TriggerEntity>,
  ) {
    super(triggersRepository);
  }
}
