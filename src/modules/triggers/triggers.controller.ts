import { ApiPaginatedResponse } from '@common/decorators';
import { PageDto, PageQueryDto } from '@common/dtos';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable, forkJoin, map } from 'rxjs';

import { TriggerCreateOneDto, TriggerDto, TriggerUpdateOneDto } from './dtos';
import { TriggersService } from './triggers.service';

@ApiTags('Triggers')
@Controller('triggers')
export class TriggersController {
  constructor(private readonly triggersService: TriggersService) {}

  @ApiResponse({ type: TriggerDto })
  @Post()
  createOne(@Body() dto: TriggerCreateOneDto): Observable<TriggerDto> {
    return this.triggersService
      .createOne(dto)
      .pipe(map((entity) => new TriggerDto(entity)));
  }

  @Get(':id')
  findOne(@Param('id', ParseUUIDPipe) id: string): Observable<TriggerDto> {
    return this.triggersService
      .findOneOrThrow({ where: { id } })
      .pipe(map((entity) => new TriggerDto(entity)));
  }

  @ApiPaginatedResponse(TriggerDto)
  @Get()
  findMany(
    @Query() { take, skip, page }: PageQueryDto,
  ): Observable<PageDto<TriggerDto>> {
    const getEntities$ = this.triggersService
      .findMany({
        take,
        skip,
      })
      .pipe(
        map((entities) => entities.map((entity) => new TriggerDto(entity))),
      );
    const countEntities$ = this.triggersService.triggersRepository.count({
      take,
      skip,
    });
    return forkJoin([getEntities$, countEntities$]).pipe(
      map(([content, count]) => {
        return new PageDto({
          content,
          take,
          page,
          total: count,
        });
      }),
    );
  }

  @Patch(':id')
  updateOne(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() dto: TriggerUpdateOneDto,
  ): Observable<TriggerDto> {
    return this.triggersService
      .updateOne({ where: { id } }, dto)
      .pipe(map((entity) => new TriggerDto(entity)));
  }

  @Delete(':id')
  removeOne(@Param('id', ParseUUIDPipe) id: string): Observable<TriggerDto> {
    return this.triggersService
      .removeOne({ where: { id } })
      .pipe(map((entity) => new TriggerDto(entity)));
  }
}
