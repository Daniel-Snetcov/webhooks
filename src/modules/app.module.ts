import { AuthModule } from '@core/auth/auth.module';
import { configModuleOptions } from '@core/config';
import { DatabaseModule } from '@core/database/database.module';
import { HealthModule } from '@core/health';
import {
  ClassSerializerInterceptor,
  Module,
  ValidationPipe,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';

import { modules } from './modules';

@Module({
  imports: [
    ConfigModule.forRoot(configModuleOptions),
    HealthModule,
    DatabaseModule,
    AuthModule,
    ...modules,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        transform: true,
        transformOptions: {
          enableImplicitConversion: true,
        },
      }),
    },
  ],
})
export class AppModule {}
