import { WebhookHeaderEntity } from '@core/database/postgres/entities';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WebhooksHeadersController } from './webhooks-headers.controller';
import { WebhooksHeadersService } from './webhooks-headers.service';

@Module({
  imports: [TypeOrmModule.forFeature([WebhookHeaderEntity])],
  controllers: [WebhooksHeadersController],
  providers: [WebhooksHeadersService],
  exports: [WebhooksHeadersService],
})
export class WebhooksHeadersModule {}
