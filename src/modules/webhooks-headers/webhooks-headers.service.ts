import { TypeormCrudRepository } from '@common/typeorm';
import { WebhookHeaderEntity } from '@core/database/postgres/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WebhooksHeadersService extends TypeormCrudRepository<WebhookHeaderEntity> {
  constructor(
    @InjectRepository(WebhookHeaderEntity)
    readonly webhooksHeadersRepository: Repository<WebhookHeaderEntity>,
  ) {
    super(webhooksHeadersRepository);
  }
}
