import { WebhookHeaderEntity } from '@core/database/postgres/entities';
import { WebhookDto } from '@modules/webhooks/dtos';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import {
  IsDate,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { randomUUID } from 'crypto';

type Fields = {
  [P in keyof WebhookHeaderEntity]: WebhookHeaderEntity[P];
};

@Expose()
export class WebhookHeaderDto implements Fields {
  constructor(partial: Partial<WebhookHeaderDto>) {
    Object.assign(this, partial);
  }

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  createdAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  updatedAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  @Exclude()
  deletedAt!: Date;

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  webhookId!: string;

  @ApiProperty({
    type: Object,
  })
  @Type(() => WebhookDto)
  @IsOptional()
  webhook?: WebhookDto;

  @ApiProperty({
    example: 'Authorization',
  })
  @IsString()
  @IsNotEmpty()
  header!: string;

  @ApiProperty({
    example: 'super_secret_token',
  })
  @IsString()
  @IsNotEmpty()
  value!: string;
}
