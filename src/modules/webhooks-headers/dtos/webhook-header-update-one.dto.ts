import { PartialType } from '@nestjs/swagger';

import { WebhookHeaderCreateOneDto } from './webhook-header-create-one.dto';

export class WebhookHeaderUpdateOneDto extends PartialType(
  WebhookHeaderCreateOneDto,
) {}
