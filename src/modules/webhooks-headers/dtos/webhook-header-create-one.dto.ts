import { PickType } from '@nestjs/swagger';

import { WebhookHeaderDto } from './webhook-header.dto';

export class WebhookHeaderCreateOneDto extends PickType(WebhookHeaderDto, [
  'header',
  'value',
] as const) {}
