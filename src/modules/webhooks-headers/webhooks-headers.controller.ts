import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable, map } from 'rxjs';

import {
  WebhookHeaderCreateOneDto,
  WebhookHeaderDto,
  WebhookHeaderUpdateOneDto,
} from './dtos';
import { WebhooksHeadersService } from './webhooks-headers.service';

@ApiTags('Webhooks Headers')
@Controller()
export class WebhooksHeadersController {
  constructor(
    private readonly webhooksHeadersService: WebhooksHeadersService,
  ) {}

  @ApiResponse({ type: WebhookHeaderDto, isArray: true })
  @Get('webhooks/:webhookId/headers')
  findMany(
    @Param('webhookId', ParseUUIDPipe) webhookId: string,
  ): Observable<WebhookHeaderDto[]> {
    return this.webhooksHeadersService
      .findMany({ where: { webhookId } })
      .pipe(
        map((entities) =>
          entities.map((entity) => new WebhookHeaderDto(entity)),
        ),
      );
  }

  @ApiResponse({ type: WebhookHeaderDto })
  @Post('webhooks/:webhookId/headers')
  createOne(
    @Param('webhookId', ParseUUIDPipe) webhookId: string,
    @Body() dto: WebhookHeaderCreateOneDto,
  ): Observable<WebhookHeaderDto> {
    return this.webhooksHeadersService
      .createOne({ ...dto, webhookId })
      .pipe(map((entity) => new WebhookHeaderDto(entity)));
  }

  @ApiResponse({ type: WebhookHeaderDto })
  @Get('webhooks-headers/:id')
  findOne(
    @Param('id', ParseUUIDPipe) id: string,
  ): Observable<WebhookHeaderDto> {
    return this.webhooksHeadersService
      .findOneOrThrow({ where: { id } })
      .pipe(map((entity) => new WebhookHeaderDto(entity)));
  }

  @ApiResponse({ type: WebhookHeaderDto })
  @Patch('webhooks-headers/:id')
  updateOne(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() dto: WebhookHeaderUpdateOneDto,
  ): Observable<WebhookHeaderDto> {
    return this.webhooksHeadersService
      .updateOne({ where: { id } }, dto)
      .pipe(map((entity) => new WebhookHeaderDto(entity)));
  }

  @ApiResponse({ type: WebhookHeaderDto })
  @Delete('webhooks-headers/:id')
  removeOne(
    @Param('id', ParseUUIDPipe) id: string,
  ): Observable<WebhookHeaderDto> {
    return this.webhooksHeadersService
      .removeOne({ where: { id } })
      .pipe(map((entity) => new WebhookHeaderDto(entity)));
  }
}
