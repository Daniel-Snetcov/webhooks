import { TypeormCrudRepository } from '@common/typeorm';
import { WebhookTriggerEntity } from '@core/database/postgres/entities';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class WebhooksTriggersService extends TypeormCrudRepository<WebhookTriggerEntity> {
  constructor(
    @InjectRepository(WebhookTriggerEntity)
    readonly webhooksTriggersRepository: Repository<WebhookTriggerEntity>,
  ) {
    super(webhooksTriggersRepository);
  }
}
