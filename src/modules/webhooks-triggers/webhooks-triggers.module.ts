import { WebhookTriggerEntity } from '@core/database/postgres/entities';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { WebhooksTriggersController } from './webhooks-triggers.controller';
import { WebhooksTriggersService } from './webhooks-triggers.service';

@Module({
  imports: [TypeOrmModule.forFeature([WebhookTriggerEntity])],
  controllers: [WebhooksTriggersController],
  providers: [WebhooksTriggersService],
  exports: [WebhooksTriggersService],
})
export class WebhooksTriggersModule {}
