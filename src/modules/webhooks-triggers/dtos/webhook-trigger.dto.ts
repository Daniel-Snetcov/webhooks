import { WebhookTriggerEntity } from '@core/database/postgres/entities';
import { TriggerDto } from '@modules/triggers';
import { WebhookDto } from '@modules/webhooks/dtos';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsDate, IsNotEmpty, IsOptional, IsUUID } from 'class-validator';
import { randomUUID } from 'crypto';

type Fields = {
  [P in keyof WebhookTriggerEntity]: WebhookTriggerEntity[P];
};

@Expose()
export class WebhookTriggerDto implements Fields {
  constructor(partial: Partial<WebhookTriggerDto>) {
    Object.assign(this, partial);
  }

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  id!: string;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  createdAt!: Date;

  @ApiProperty()
  @IsDate()
  @IsNotEmpty()
  @Exclude()
  deletedAt!: Date;

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  triggerId!: string;

  @ApiProperty({
    type: TriggerDto,
  })
  @Type(() => TriggerDto)
  @IsOptional()
  trigger?: TriggerDto | undefined;

  @ApiProperty({
    example: randomUUID(),
  })
  @IsUUID()
  @IsNotEmpty()
  webhookId!: string;

  @ApiProperty({
    type: WebhookDto,
  })
  @Type(() => WebhookDto)
  @IsOptional()
  webhook?: WebhookDto | undefined;
}
