import { PickType } from '@nestjs/swagger';

import { WebhookTriggerDto } from './webhook-trigger.dto';

export class WebhookTriggerCreateOneDto extends PickType(WebhookTriggerDto, [
  'triggerId',
  'webhookId',
] as const) {}
