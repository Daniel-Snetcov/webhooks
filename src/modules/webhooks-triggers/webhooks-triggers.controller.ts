import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseUUIDPipe,
  Post,
} from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Observable, concatMap, from, map } from 'rxjs';

import { WebhookTriggerCreateOneDto, WebhookTriggerDto } from './dtos';
import { WebhooksTriggersService } from './webhooks-triggers.service';

@ApiTags('Webhooks Triggers')
@Controller()
export class WebhooksTriggersController {
  constructor(
    private readonly webhooksTriggersService: WebhooksTriggersService,
  ) {}

  @ApiResponse({ type: WebhookTriggerDto })
  @Post('webhooks-triggers')
  createOne(
    @Body() dto: WebhookTriggerCreateOneDto,
  ): Observable<WebhookTriggerDto> {
    return this.webhooksTriggersService.createOne(dto).pipe(
      concatMap((entity) =>
        from(
          this.webhooksTriggersService.findOneOrThrow({
            where: { id: entity.id },
            relations: { webhook: true, trigger: true },
          }),
        ),
      ),
      map((entity) => new WebhookTriggerDto(entity)),
    );
  }

  @ApiResponse({ type: WebhookTriggerDto, isArray: true })
  @Get('webhooks/:webhooksId/triggers')
  findByWebhook(): Observable<WebhookTriggerDto[]> {
    return this.webhooksTriggersService
      .findMany({ relations: { webhook: true, trigger: true } })
      .pipe(
        map((entities) =>
          entities.map((entity) => new WebhookTriggerDto(entity)),
        ),
      );
  }

  @ApiResponse({ type: WebhookTriggerDto })
  @Delete('webhooks-triggers/:id')
  removeOne(
    @Param('id', ParseUUIDPipe) id: string,
  ): Observable<WebhookTriggerDto> {
    return this.webhooksTriggersService
      .removeOne(
        { where: { id }, relations: { webhook: true, trigger: true } },
        false,
      )
      .pipe(map((entity) => new WebhookTriggerDto(entity)));
  }
}
