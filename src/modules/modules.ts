import { TriggersModule } from './triggers/triggers.module';
import { WebhooksModule } from './webhooks/webhooks.module';
import { WebhooksHeadersModule } from './webhooks-headers/webhooks-headers.module';
import { WebhooksRequestsAttemptsModule } from './webhooks-requests-attemps/webhooks-requests-attempts.module';
import { WebhooksRequestsAttemptsLogsModule } from './webhooks-requests-attempts-logs/webhooks-requests-attempts-logs.module';
import { WebhooksTriggersModule } from './webhooks-triggers/webhooks-triggers.module';

export const modules = [
  WebhooksModule,
  WebhooksHeadersModule,
  TriggersModule,
  WebhooksTriggersModule,
  WebhooksRequestsAttemptsModule,
  WebhooksRequestsAttemptsLogsModule,
] as const;
