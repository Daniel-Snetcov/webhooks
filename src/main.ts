import { Logger } from '@nestjs/common';
import * as cluster from 'cluster';
import * as os from 'os';

import './exit-handler';
import { bootstrap } from './bootstrap';

/** Cluster configuration */
const runningOnCluster: boolean = process.env.NODE_ENV !== 'local';
if (runningOnCluster) {
  const clusterInstance = cluster as unknown as cluster.Cluster;

  if (clusterInstance.isPrimary) {
    const cpuCount = os.cpus().length;

    for (let i = 0; i < cpuCount; i += 1) {
      clusterInstance.fork();
    }

    clusterInstance.on('online', (worker) => {
      Logger.log(`${worker.process.pid} is online`, 'NestApplication::Cluster');
    });

    clusterInstance.on('exit', ({ process }) => {
      Logger.log(`${process.pid} died`, 'NestApplication::Cluster');
    });
  } else {
    bootstrap().catch(() => {
      Logger.error(
        `${process.pid} could not bootstrap`,
        'NestApplication::Cluster',
      );
    });
  }
} else {
  bootstrap().catch(() => {
    Logger.error(
      `${process.pid} could not bootstrap`,
      'NestApplication::Cluster',
    );
  });
}
