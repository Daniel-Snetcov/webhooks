import { ProcessEnv as ProcessEnvType } from '@common/types/process-env.type';

export declare global {
  namespace NodeJS {
    type ProcessEnv = ProcessEnvType;
  }
}
