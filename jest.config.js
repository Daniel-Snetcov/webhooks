module.exports = {
  displayName: 'webhooks',
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: 'src',
  testRegex: '.*\\.spec\\.ts$',
  maxWorkers: 1,
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  collectCoverageFrom: ['**/*.(t|j)s'],
  coveragePathIgnorePatterns: [
    'interfaces',
    '.module.ts',
    '.mock.ts',
    'index.ts',
  ],
  coverageDirectory: '../coverage',
  coverageReporters: ['text', 'html', 'lcov'],
  testEnvironment: 'node',
  passWithNoTests: true,
  moduleNameMapper: {
    '^@common$': '<rootDir>/common',
    '^@common/(.*)$': '<rootDir>/common/$1',
    '^@core$': '<rootDir>/core',
    '^@core/(.*)$': '<rootDir>/core/$1',
    '^@modules$': '<rootDir>/modules',
    '^@modules/(.*)$': '<rootDir>/modules/$1',
  },
};
