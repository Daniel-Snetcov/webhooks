import { AppModule } from '@modules/app.module';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';

interface HealthResponse extends request.Response {
  body: {
    status: string;
    info: {
      Postgres: {
        status: string;
      };
    };
  };
}

describe('HealthController (e2e)', () => {
  let app: INestApplication;
  let server: request.SuperTest<request.Test>;
  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    server = request(app.getHttpServer());
    await app.init();
  });
  afterAll(async () => {
    await app.close();
  });
  it('/health (GET)', async () => {
    const response: HealthResponse = await server.get('/health').expect(200);
    expect(response.body).toHaveProperty('status', 'ok');
  });
});
