module.exports = {
  displayName: 'webhooks',
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '.',
  testRegex: '.*\\.e2e-spec\\.ts$',
  maxWorkers: 1,
  testTimeout: 10_000,
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  moduleNameMapper: {
    '^@common$': '<rootDir>/src/common',
    '^@common/(.*)$': '<rootDir>/src/common/$1',
    '^@core$': '<rootDir>/src/core',
    '^@core/(.*)$': '<rootDir>/src/core/$1',
    '^@modules$': '<rootDir>/src/modules',
    '^@modules/(.*)$': '<rootDir>/src/modules/$1',
  },
  coveragePathIgnorePatterns: [
    'interfaces',
    '.module.ts',
    '.mock.ts',
    'index.ts',
    '<rootDir>/main.ts',
    '<rootDir>/core/config',
    '<rootDir>/common/types',
    '<rootDir>/common/exceptions',
  ],
};
